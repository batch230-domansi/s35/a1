const express = require("express");

const mongoose = require("mongoose");
const app = express();
const port = 3001;

mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.yat49rm.mongodb.net/s35?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
)

/*
connection to database
allows to handle erros when the initial connection is established
works with the on and once mongoose methods
*/
let db = mongoose.connection

/*
if a connection errors occured, output in the console

console.error.bind(console) allows us to print errors in the browser console and in the terminal
*/

db.on("error",console.error.bind(console,"connection error"));
db.once("open",()=> console.log("We're connected to the cloud database"));


app.use(express.json());

const SignUpSchema = new mongoose.Schema({
	username: String,
	password:{
		type: String,
	    default: "pending"
	}
})

const User = mongoose.model("User", SignUpSchema);

app.post("/signup", (request, response) => {

	User.findOne({username: request.body.username}, (err, result) => {

		if(result != null && result.username == request.body.username && result.password == request.body.password){
			return response.send("Duplicate SignUp found");
		}
		else{

			let newUser = new User({
					username: request.body.username,
		            password: request.body.password
			})

			newUser.save((saveErr, savednewUser)=> {
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return response.status(201).send("New user registered");
				}
			})
		}	
	})
})


app.get("/signup", (request, response) => {

	User.find({}, (err, result) => {
		if(err){
			return console.log(err);
		}
		else{
			return response.status(200).json({
				data : result
			})
		}

	})
})



app.listen(port, ()=> console.log(`Server running at port' ${port}`));




/*
Activity:
1. Create a User schema.
2. Create a User model.
3. Create a POST route that will access the "/signup" route that will create a user.
4. Process a POST request at the "/signup" route using postman to register a user.
5. Create a git repository named S35.
6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
7. Add the link in Boodle.

*/


/*
1. Create a request via Postman to retrieve all the to do list items.
- GET HTTP method
- https://jsonplaceholder.typicode.com/todos URI endpoint
- Save this request as get all to do list items
2. Create a request via Postman to retrieve an individual to do list item.
- GET HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as get to do list item
3. Create a request via Postman to create a to do list item.
- POST HTTP method
- https://jsonplaceholder.typicode.com/todos URI endpoint
- Save this request as create to do list item
4. Create a request via Postman to update a to do list item.
- PUT HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as update to do list item PUT
- Update the to do list item to mirror the data structure used in the PUT fetch request
5. Create a request via Postman to update a to do list item.
- PATCH HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as create to do list item
- Update the to do list item to mirror the data structure of the PATCH fetch request
6. Create a request via Postman to delete a to do list item.
- DELETE HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as delete to do list item
7. Export the Postman collection and save it in the a1 folder.
*/